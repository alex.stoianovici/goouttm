# goouttm

Prerequisites :
Visual Studio 2022 : https://visualstudio.microsoft.com/vs/
Node.js : https://nodejs.org/en
Microsoft SQL Server Management Studio : https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16

How to launch the backend: 
Step 1 : open GoOut.sln from GoOut folder
Step 2 : make sure GoOut.API is selected for the debugging button
Step 3 : run debug
Now, a debugging command line and a Swagger web window should appear.

How to launch the frontend:
Step 1 : navigate with command line to the gooutf folder
Step 2 : run npm install
Step 3 : run npm start
Now, a web window for GoOutTM will appear.

How to access the database : 
Step 1 : Enter "(LocalDb)\gooutdatabase" as Server Name and make sure Database Engine is selected at Server Type and Windows Authentication is choosed for Authentication
Step 2 : Navigate to the database from the left side "Object Explorer"
